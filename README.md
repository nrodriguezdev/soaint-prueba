# soaint-prueba
### Pasos:
1. Crear el archivo de configuracion.

        Para despliegue de produccion:

        Copiar el archivo 

        soaint-prueba/src/main/resources/application-default.yml a la siguiente ruta:

        C:\\soaint\\application-prod.yml

        Una vez copiado cambiar de nombre a application-prod.yml

        Nota: El archivo application-prod.yml se puede modificar con acorde al Usuario
        

2. Crear el fichero txt de los usuarios:

        Ruta:

            C:\\soaint\\credenciales.txt

        Estructura:
        
        usuario,contrasenia,nombre,apellido,dni,telefono,email

        Ejemplo:

        nrodriguez,123456,nilton,rodriguez,75625135,936383226,nrodriguezdev@gmail.com
        vsotelo,123789,gustavo,ruiz,75625111,999555987,gruiz@example.com

3. Compilar el proyecto

        mvn clean package

4. Generar imagen
    
        docker build . -t soaint-image

5. Despliegues:
    
        Despliegue a produccion:

        docker-compose -f docker-compose.prod.yml up

        Despliegues a desarrollo:

        docker-compose up

6. Probar los servicios con swagger:

        http://localhost:9797/soaint/swagger-ui.html


