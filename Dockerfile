FROM openjdk:8-jdk-alpine

EXPOSE 9797

ARG JAR_FILE=/*.jar
ADD ${JAR_FILE} app.jar

ENTRYPOINT java -Dspring.profiles.active=${PROFILING} -jar /app.jar
