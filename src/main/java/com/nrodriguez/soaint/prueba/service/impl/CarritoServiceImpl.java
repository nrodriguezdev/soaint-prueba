package com.nrodriguez.soaint.prueba.service.impl;

import com.nrodriguez.soaint.prueba.exception.CarritoException;
import com.nrodriguez.soaint.prueba.model.Cliente;
import com.nrodriguez.soaint.prueba.model.DetalleVenta;
import com.nrodriguez.soaint.prueba.model.Producto;
import com.nrodriguez.soaint.prueba.model.Venta;
import com.nrodriguez.soaint.prueba.model.expose.DetalleCarritoRequest;
import com.nrodriguez.soaint.prueba.model.expose.VentaDetalleResponse;
import com.nrodriguez.soaint.prueba.model.expose.VentaHeaderResponse;
import com.nrodriguez.soaint.prueba.model.expose.VentaResponse;
import com.nrodriguez.soaint.prueba.persistence.ClienteRepository;
import com.nrodriguez.soaint.prueba.persistence.DetalleVentaRepository;
import com.nrodriguez.soaint.prueba.persistence.ProductoRepository;
import com.nrodriguez.soaint.prueba.persistence.VentaRepository;
import com.nrodriguez.soaint.prueba.service.CarritoService;
import io.reactivex.Single;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarritoServiceImpl implements CarritoService {

    private final VentaRepository ventaRepository;
    private final DetalleVentaRepository detalleVentaRepository;
    private final ProductoRepository productoRepository;
    private final ClienteRepository clienteRepository;

    @Autowired
    public CarritoServiceImpl(VentaRepository ventaRepository, DetalleVentaRepository detalleVentaRepository, ProductoRepository productoRepository, ClienteRepository clienteRepository) {
        this.ventaRepository = ventaRepository;
        this.detalleVentaRepository = detalleVentaRepository;
        this.productoRepository = productoRepository;
        this.clienteRepository = clienteRepository;
    }

    @Override
    public VentaResponse createVenta(Long idCliente) throws CarritoException {
        try {
            Venta venta = new Venta();
            venta.setFecha(new Date().getTime());
            venta.setIdCliente(idCliente);
            ventaRepository.save(venta);
            return VentaResponse.builder()
                    .fecha(venta.getFecha())
                    .idCliente(venta.getIdCliente())
                    .idVenta(venta.getIdVenta())
                    .build();
        } catch (Exception e) {
            throw new CarritoException(HttpStatus.INTERNAL_SERVER_ERROR, "Hubo un error al intentar crear una venta", e.getMessage(), "0003");
        }
    }

    @Override
    public String agregarProductoAlCarrito(DetalleCarritoRequest request) throws CarritoException {
        try {
            if (productoRepository.findById(request.getIdProducto()).isPresent() && ventaRepository.findById(request.getIdVenta()).isPresent()) {
                DetalleVenta detalleVenta = new DetalleVenta();
                detalleVenta.setIdProducto(request.getIdProducto());
                detalleVenta.setIdVenta(request.getIdVenta());
                detalleVentaRepository.save(detalleVenta);
                return "Se agrego producto satisfactoriamente";
            } else {
                throw new CarritoException(HttpStatus.BAD_REQUEST, "Verifique request enviado.", "87382", "0003");
            }
        } catch (CarritoException e) {
            throw new CarritoException(e.getHttpStatus(), e.getMessage(), e.getBackendMessage(), e.getCode());
        } catch (Exception e) {
            throw new CarritoException(HttpStatus.INTERNAL_SERVER_ERROR, "Hubo un error al intentar agregar un producto a la venta.", e.getMessage(), "0003");
        }
    }

    @Override
    public Single<VentaHeaderResponse> findByIdVenta(Long idVenta) throws CarritoException {
        return Single.create(singleEmitter -> {
            Optional<Venta> ventaOptional = ventaRepository.findById(idVenta);
            if (ventaOptional.isPresent()) {
                VentaHeaderResponse ventaHeaderResponse = new VentaHeaderResponse();
                Venta venta = ventaOptional.get();
                Cliente cliente = clienteRepository.findById(venta.getIdCliente()).get();
                ventaHeaderResponse.setIdCliente(venta.getIdCliente());
                ventaHeaderResponse.setIdVenta(venta.getIdVenta());
                ventaHeaderResponse.setNombreCliente(cliente.getNombreCliente());
                ventaHeaderResponse.setFechaVenta(venta.getFecha());
                ventaHeaderResponse.setLstDetalle(
                        detalleVentaRepository.findAllDetalleByIdVenta(venta.getIdVenta()).stream().map(this::parseDetalleVentaToVentaDetalleResponse).collect(Collectors.toList())
                );
                singleEmitter.onSuccess(ventaHeaderResponse);
            } else {
                throw new CarritoException(HttpStatus.BAD_REQUEST, "Venta no existe.", "87382", "0003");
            }
        });
    }

    @Override
    public Single<List<VentaHeaderResponse>> findByIdCliente(Long idCliente) throws CarritoException {
        return Single.create(singleEmitter -> {
            List<Venta> listVenta = ventaRepository.buscarVentasPorIdCliente(idCliente);
            if (CollectionUtils.isEmpty(listVenta)) {
                throw new CarritoException(HttpStatus.BAD_REQUEST, "Cliente no tiene ventas.", "87382", "0003");
            }
            List<VentaHeaderResponse> result = new ArrayList<>();
            VentaHeaderResponse ventaHeaderResponse;
            for (Venta venta : listVenta) {
                ventaHeaderResponse = new VentaHeaderResponse();
                Cliente cliente = clienteRepository.findById(venta.getIdCliente()).get();
                ventaHeaderResponse.setIdCliente(venta.getIdCliente());
                ventaHeaderResponse.setIdVenta(venta.getIdVenta());
                ventaHeaderResponse.setNombreCliente(cliente.getNombreCliente());
                ventaHeaderResponse.setFechaVenta(venta.getFecha());
                ventaHeaderResponse.setLstDetalle(
                        detalleVentaRepository.findAllDetalleByIdVenta(venta.getIdVenta()).stream().map(this::parseDetalleVentaToVentaDetalleResponse).collect(Collectors.toList())
                );
                result.add(ventaHeaderResponse);
            }
            singleEmitter.onSuccess(result);
        });
    }

    private VentaDetalleResponse parseDetalleVentaToVentaDetalleResponse(DetalleVenta detalleVenta) {
        VentaDetalleResponse detalleResponse = new VentaDetalleResponse();
        detalleResponse.setIdDetalleVenta(detalleVenta.getIdDetalleVenta());
        detalleResponse.setIdProducto(detalleVenta.getIdProducto());
        Producto producto = productoRepository.findById(detalleResponse.getIdProducto()).get();
        detalleResponse.setNombreProducto(producto.getNombre());
        detalleResponse.setPrecioProducto(producto.getPrecio());
        return detalleResponse;
    }

}
