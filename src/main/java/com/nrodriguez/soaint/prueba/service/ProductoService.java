package com.nrodriguez.soaint.prueba.service;

import com.nrodriguez.soaint.prueba.exception.CarritoException;
import com.nrodriguez.soaint.prueba.model.Producto;
import com.nrodriguez.soaint.prueba.model.expose.ProductoRequest;

import java.util.List;

public interface ProductoService {

    Producto registrarProducto(ProductoRequest request) throws CarritoException;
    String eliminarProducto(Long id) throws CarritoException;
    Producto consultarProducto(Long id) throws CarritoException;
    List<Producto> buscarTodosProducto() throws CarritoException;
}
