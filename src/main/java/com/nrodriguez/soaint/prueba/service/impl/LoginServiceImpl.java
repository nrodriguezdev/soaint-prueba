package com.nrodriguez.soaint.prueba.service.impl;

import com.nrodriguez.soaint.prueba.config.AppConfig;
import com.nrodriguez.soaint.prueba.exception.LoginException;
import com.nrodriguez.soaint.prueba.model.Cliente;
import com.nrodriguez.soaint.prueba.model.CuentaDeCliente;
import com.nrodriguez.soaint.prueba.model.expose.TokenResponse;
import com.nrodriguez.soaint.prueba.model.expose.UserCredentialsRequest;
import com.nrodriguez.soaint.prueba.persistence.ClienteRepository;
import com.nrodriguez.soaint.prueba.persistence.CuentaDeClienteRepository;
import com.nrodriguez.soaint.prueba.service.LoginService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LoginServiceImpl implements LoginService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginServiceImpl.class);


    private final AppConfig appConfig;
    private final ClienteRepository clienteRepository;
    private final CuentaDeClienteRepository cuentaDeClienteRepository;

    @Autowired
    public LoginServiceImpl(AppConfig appConfig, ClienteRepository clienteRepository, CuentaDeClienteRepository cuentaDeClienteRepository) {
        this.appConfig = appConfig;
        this.clienteRepository = clienteRepository;
        this.cuentaDeClienteRepository = cuentaDeClienteRepository;
    }

    @Override
    public TokenResponse login(UserCredentialsRequest request) throws LoginException {
        CuentaDeCliente cuentaDeCliente = cuentaDeClienteRepository.findByUsername(request.getUsername());
        if (cuentaDeCliente == null) {
            LOGGER.info("[DEBUG] message -> {}", "Datos incorrectos en el logeo");
            throw new LoginException(HttpStatus.FORBIDDEN, "Usuario No Existe", "0003", "0004");
        }
        if (!StringUtils.equals(cuentaDeCliente.getPassword(), request.getPassword())) {
            LOGGER.info("[DEBUG] message -> {}", "Datos incorrectos en el logeo");
            throw new LoginException(HttpStatus.FORBIDDEN, "Contraseña incorrecta", "0003", "0004");
        }

        try {
            Cliente cliente = clienteRepository.findById(cuentaDeCliente.getIdCliente()).get();
            Date now = new Date();
            Date validity = new Date(now.getTime() + appConfig.EXPIRY_TIME_TOKEN);

            Map<String, Object> claims = new HashMap<>();
            claims.put("idCliente", cliente.getIdCliente());
            claims.put("nombreCliente", cliente.getNombreCliente());
            claims.put("apellido", cliente.getApellido());
            claims.put("dni", cliente.getDni());
            claims.put("telefono", cliente.getTelefono());
            claims.put("email", cliente.getEmail());

            String _token = Jwts.builder()
                    .setSubject(request.getUsername())
                    .setClaims(claims)
                    .setIssuedAt(now)
                    .setExpiration(validity)
                    .signWith(SignatureAlgorithm.HS512, appConfig.SECRET_KEY).compact();

            LOGGER.debug("[DEBUG] message -> {}", "Logeo Exitoso");
            return TokenResponse.builder().token(_token).build();
        } catch (Exception e) {
            LOGGER.error("[ERROR] message -> {}", e.getMessage());
            throw new LoginException(HttpStatus.FORBIDDEN, "Error interno", e.getMessage(), "0004");
        }
    }

    @Override
    public Cliente decodeToken(String token) throws LoginException {
        try {
            Claims claims = Jwts.parser().setSigningKey(appConfig.SECRET_KEY).parseClaimsJws(token).getBody();
            Cliente result = new Cliente();
            result.setIdCliente(MapUtils.getLong(claims, "idCliente"));
            result.setNombreCliente(MapUtils.getString(claims, "nombreCliente"));
            result.setApellido(MapUtils.getString(claims, "apellido"));
            result.setDni(MapUtils.getString(claims, "dni"));
            result.setTelefono(MapUtils.getString(claims, "telefono"));
            result.setEmail(MapUtils.getString(claims, "email"));
            return result;
        } catch (ExpiredJwtException ex) {
            throw new LoginException(HttpStatus.FORBIDDEN, "Token Enviado expiro", "0003", "0004");
        } catch (Exception e) {
            throw new LoginException(HttpStatus.FORBIDDEN, "Hubo un error en el token enviado", "0003", "0004");
        }
    }

}