package com.nrodriguez.soaint.prueba.service;

import com.nrodriguez.soaint.prueba.exception.LoginException;
import com.nrodriguez.soaint.prueba.model.Cliente;
import com.nrodriguez.soaint.prueba.model.expose.TokenResponse;
import com.nrodriguez.soaint.prueba.model.expose.UserCredentialsRequest;

public interface LoginService {

    TokenResponse login(UserCredentialsRequest request) throws LoginException;
    Cliente decodeToken(String token) throws LoginException;

}
