package com.nrodriguez.soaint.prueba.service.impl;

import com.nrodriguez.soaint.prueba.exception.CarritoException;
import com.nrodriguez.soaint.prueba.model.Producto;
import com.nrodriguez.soaint.prueba.model.expose.ProductoRequest;
import com.nrodriguez.soaint.prueba.persistence.ProductoRepository;
import com.nrodriguez.soaint.prueba.service.ProductoService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductoServiceImpl implements ProductoService {

    private final ProductoRepository productoRepository;

    @Autowired
    public ProductoServiceImpl(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    @Override
    public Producto registrarProducto(ProductoRequest request) throws CarritoException {
        try {
            Producto producto = new Producto();
            producto.setNombre(request.getNombreProducto());
            producto.setPrecio(request.getPrecioProducto());
            return productoRepository.save(producto);
        } catch (Exception e) {
            throw new CarritoException(HttpStatus.INTERNAL_SERVER_ERROR, "Hubo un error al intentar registrar una producto", e.getMessage(), "0003");
        }
    }

    @Override
    public String eliminarProducto(Long id) throws CarritoException {
        Optional<Producto> producto = productoRepository.findById(id);
        if (producto.isPresent()) {
            productoRepository.deleteById(id);
            return "Eliminado satisfactoriamente";
        } else {
            throw new CarritoException(HttpStatus.BAD_REQUEST, "Hubo un error al intentar eliminar el prducto enviado", "00012", "0003");
        }
    }

    @Override
    public Producto consultarProducto(Long id) throws CarritoException {
        Optional<Producto> producto = productoRepository.findById(id);
        if (producto.isPresent()) {
            return producto.get();
        } else {
            throw new CarritoException(HttpStatus.BAD_REQUEST, "Hubo un error al intentar buscar prducto enviado", "00012", "0003");
        }
    }

    @Override
    public List<Producto> buscarTodosProducto() throws CarritoException {
        List<Producto> lstProductos = productoRepository.findAll();
        if (!CollectionUtils.isEmpty(lstProductos)) {
            return lstProductos;
        } else {
            return new ArrayList<>();
        }
    }
}
