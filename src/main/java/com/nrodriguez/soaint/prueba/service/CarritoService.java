package com.nrodriguez.soaint.prueba.service;

import com.nrodriguez.soaint.prueba.exception.CarritoException;
import com.nrodriguez.soaint.prueba.model.expose.DetalleCarritoRequest;
import com.nrodriguez.soaint.prueba.model.expose.VentaHeaderResponse;
import com.nrodriguez.soaint.prueba.model.expose.VentaResponse;
import io.reactivex.Single;

import java.util.List;

public interface CarritoService {

    VentaResponse createVenta(Long idCliente) throws CarritoException;
    String agregarProductoAlCarrito(DetalleCarritoRequest request) throws CarritoException;
    Single<VentaHeaderResponse> findByIdVenta(Long idVenta)throws CarritoException;
    Single<List<VentaHeaderResponse>> findByIdCliente(Long idCliente) throws CarritoException;


}
