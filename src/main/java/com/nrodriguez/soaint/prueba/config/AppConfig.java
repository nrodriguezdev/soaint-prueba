package com.nrodriguez.soaint.prueba.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {

    @Value("${security.config-credentials.patch}")
    public String PATH_CREDENTIALS_USERS;

    @Value("${security.config-credentials.separator}")
    public String FILE_CREDENTIALS_SEPARATOR_USERS;

    @Value("${security.jwt.expiration}")
    public Long EXPIRY_TIME_TOKEN;

    @Value("${security.jwt.secret}")
    public String SECRET_KEY;
}
