package com.nrodriguez.soaint.prueba;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nrodriguez.soaint.prueba.config.AppConfig;
import com.nrodriguez.soaint.prueba.exception.LoginException;
import com.nrodriguez.soaint.prueba.model.Cliente;
import com.nrodriguez.soaint.prueba.model.CuentaDeCliente;
import com.nrodriguez.soaint.prueba.model.dto.DataUsersDto;
import com.nrodriguez.soaint.prueba.model.expose.UserCredentialsRequest;
import com.nrodriguez.soaint.prueba.persistence.ClienteRepository;
import com.nrodriguez.soaint.prueba.persistence.CuentaDeClienteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class OnLoadDataUsers {

    private static final Logger LOGGER = LoggerFactory.getLogger(OnLoadDataUsers.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().create();

    private final AppConfig appConfig;
    private final ClienteRepository clienteRepository;
    private final CuentaDeClienteRepository cuentaDeClienteRepository;

    @Autowired
    public OnLoadDataUsers(AppConfig appConfig, ClienteRepository clienteRepository, CuentaDeClienteRepository cuentaDeClienteRepository) {
        this.appConfig = appConfig;
        this.clienteRepository = clienteRepository;
        this.cuentaDeClienteRepository = cuentaDeClienteRepository;
    }

    public void load() {
        try {
            List<DataUsersDto> lstDataUsersDto = readFileUsers();
            Cliente cliente;
            CuentaDeCliente cuentaDeCliente;
            for (DataUsersDto data : lstDataUsersDto) {
                cliente = new Cliente();
                cliente.setNombreCliente(data.getNombreCliente());
                cliente.setApellido(data.getApellido());
                cliente.setDni(data.getDni());
                cliente.setTelefono(data.getTelefono());
                cliente.setEmail(data.getEmail());
                clienteRepository.save(cliente);
                cuentaDeCliente = new CuentaDeCliente();
                cuentaDeCliente.setIdCliente(cliente.getIdCliente());
                cuentaDeCliente.setUsername(data.getUsuario());
                cuentaDeCliente.setPassword(data.getContrasenia());
                cuentaDeClienteRepository.save(cuentaDeCliente);
                LOGGER.info("[CLIENTE REGISTRADO -> {}, con cuenta de Usuario -> {}]", GSON.toJson(cliente), cuentaDeCliente.getUsername());
            }
        } catch (Exception e) {
            LOGGER.error("[ERROR] Exception -> {},{}", e, e.getMessage());
        }
    }

    private List<DataUsersDto> readFileUsers() throws LoginException {
        try {
            List<String> userListString = new ArrayList<>();
            Stream<String> fileLines = null;
            Path path = Paths.get(appConfig.PATH_CREDENTIALS_USERS);
            fileLines = Files.lines(path, StandardCharsets.UTF_8);
            fileLines.forEach(userListString::add);
            return userListString.parallelStream().map(this::parseStringUsers).collect(Collectors.toList());
        } catch (Exception e) {
            throw new LoginException(HttpStatus.INTERNAL_SERVER_ERROR, "Error al leer el archivo de usuarios", e.getMessage(), "0002");
        }
    }

    private DataUsersDto parseStringUsers(String valor) {
        String[] lstUsersString = valor.split(appConfig.FILE_CREDENTIALS_SEPARATOR_USERS);
        return DataUsersDto.builder()
                .usuario(lstUsersString[0])
                .contrasenia(lstUsersString[1])
                .nombreCliente(lstUsersString[2])
                .apellido(lstUsersString[3])
                .dni(lstUsersString[4])
                .telefono(lstUsersString[5])
                .email(lstUsersString[6])
                .build();
    }

}
