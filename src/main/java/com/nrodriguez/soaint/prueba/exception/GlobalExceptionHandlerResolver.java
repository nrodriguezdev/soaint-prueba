package com.nrodriguez.soaint.prueba.exception;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandlerResolver {

    @ExceptionHandler({Exception.class, LoginException.class, CarritoException.class})
    public ResponseEntity<CustomResponseException> customException(Exception e) {
        e.printStackTrace();
        if (e instanceof LoginException) {
            LoginException loginException = (LoginException) e;
            CustomResponseException responseException =
                    CustomResponseException.builder()
                            .httpStatus(loginException.getHttpStatus())
                            .message(loginException.getMessage())
                            .backendMessage(loginException.getBackendMessage())
                            .code(loginException.getHttpStatus().toString())
                            .build();
            return new ResponseEntity<CustomResponseException>(responseException, loginException.getHttpStatus());
        } else if (e instanceof CarritoException) {
            CarritoException loginException = (CarritoException) e;
            CustomResponseException responseException =
                    CustomResponseException.builder()
                            .httpStatus(loginException.getHttpStatus())
                            .message(loginException.getMessage())
                            .backendMessage(loginException.getBackendMessage())
                            .code(loginException.getHttpStatus().toString())
                            .build();
            return new ResponseEntity<CustomResponseException>(responseException, loginException.getHttpStatus());
        } else {
            CustomResponseException responseException =
                    CustomResponseException.builder()
                            .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                            .message("Generic Error")
                            .backendMessage("Generic error")
                            .code("9999")
                            .build();
            return new ResponseEntity<CustomResponseException>(responseException, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Builder
    @Data
    private static class CustomResponseException {
        private HttpStatus httpStatus;
        private String message;
        private String backendMessage;
        private String code;
    }
}
