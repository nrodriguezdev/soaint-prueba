package com.nrodriguez.soaint.prueba.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class CarritoException extends Exception {

    private HttpStatus httpStatus;
    private String message;
    private String backendMessage;
    private String code;

    public CarritoException(HttpStatus httpStatus, String message, String backendMessage, String code) {
        super(message);
        this.httpStatus = httpStatus;
        this.message = message;
        this.backendMessage = backendMessage;
        this.code = code;
    }
}
