package com.nrodriguez.soaint.prueba;

import com.nrodriguez.soaint.prueba.config.YamlPropertySourceFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@ConfigurationPropertiesScan("com.nrodriguez.soaint.prueba.config")
@EnableConfigurationProperties
@PropertySources({
        // Archivo de configuracion Desarrollo
        @PropertySource(value = "classpath:/application-default.yml", encoding = "UTF-8", factory = YamlPropertySourceFactory.class),
        // Archivo de configuracion Externo
		@PropertySource(value = "file:C:\\soaint\\application-prod.yml", encoding = "UTF-8", factory = YamlPropertySourceFactory.class,
				ignoreResourceNotFound = true)
})
public class SoaintPruebaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SoaintPruebaApplication.class, args);
    }

    @Bean
    CommandLineRunner init(OnLoadDataUsers service) {
        return (args) -> {
            service.load();
        };
    }

}
