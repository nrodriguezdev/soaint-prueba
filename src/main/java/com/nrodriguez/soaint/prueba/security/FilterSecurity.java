package com.nrodriguez.soaint.prueba.security;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nrodriguez.soaint.prueba.model.Cliente;
import com.nrodriguez.soaint.prueba.service.LoginService;
import com.nrodriguez.soaint.prueba.utils.Constants;
import com.nrodriguez.soaint.prueba.utils.enums.EnumTokenExceptions;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Optional;

@Component
@Order(1)
public class FilterSecurity implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(FilterSecurity.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().create();
    private static final String APPLICATION_JSON = "application/json";
    private static final String UTF8 = "UTF-8";

    private final LoginService loginService;

    @Autowired
    public FilterSecurity(LoginService loginService) {
        this.loginService = loginService;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) servletRequest;
        final HttpServletResponse res = (HttpServletResponse) servletResponse;

        if (req.getRequestURI().contains("swagger") || req.getRequestURI().contains("api-docs") || req.getRequestURI().contains("login")) {
            filterChain.doFilter(req, res);
            return;
        }

        final String authHeader = Optional.ofNullable(req.getHeader(HttpHeaders.AUTHORIZATION)).orElse(StringUtils.EMPTY);
        if (authHeader.isEmpty()) {
            generateResult(res, HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN.toString(), EnumTokenExceptions.ERROR_TOKEN.getMessageError());
            return;
        }


        try {
            Cliente cliente = loginService.decodeToken(authHeader);
            setterHeader(cliente, res, req);
            filterChain.doFilter(req, res);
            LOG.info("[INFO] Cliente autenticado -> {}", GSON.toJson(cliente));
        } catch (ExpiredJwtException ex) {
            generateResult(res, HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN.toString(), EnumTokenExceptions.ERROR_TOKEN_EXPIRED.getMessageError());
        } catch (Exception e) {
            generateResult(res, HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN.toString(), EnumTokenExceptions.ERROR_DECODE.getMessageError());
        }
    }

    private void generateResult(HttpServletResponse httpServletResponse, Integer status, String code, String message) {
        try {

            httpServletResponse.setStatus(status);
            httpServletResponse.setContentType(Constants.APPLICATION_JSON);
            httpServletResponse.setCharacterEncoding(Constants.UTF8);

            LinkedHashMap<String, Object> mapError = new LinkedHashMap<>();
            mapError.put(Constants.KEY_CODE, code);
            mapError.put(Constants.KEY_MESSAGE, message);
            mapError.put(Constants.KEY_HTTP_STATUS, status);

            PrintWriter out = httpServletResponse.getWriter();
            out.print(GSON.toJson(mapError));
            out.flush();
            IOUtils.closeQuietly(out);

        } catch (IOException e) {
            LOG.error("Error Print Error Authentication: ");
        }
    }

    private void setterHeader(Cliente cliente, HttpServletResponse res, HttpServletRequest req) {
        res.setContentType(APPLICATION_JSON);
        res.setCharacterEncoding(UTF8);
        res.setStatus(HttpServletResponse.SC_OK);
        res.addHeader("idCliente", cliente.getIdCliente().toString());
        req.setAttribute("idCliente", cliente.getIdCliente().toString());
    }
}
