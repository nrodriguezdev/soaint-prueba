package com.nrodriguez.soaint.prueba.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cliente")
@Getter
@Setter
public class Cliente {

    @Id
    @GeneratedValue
    private Long idCliente;

    private String nombreCliente;

    private String apellido;

    private String dni;

    private String telefono;

    private String email;
}
