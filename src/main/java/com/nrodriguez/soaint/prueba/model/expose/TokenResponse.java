package com.nrodriguez.soaint.prueba.model.expose;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TokenResponse {

    private String token;
}
