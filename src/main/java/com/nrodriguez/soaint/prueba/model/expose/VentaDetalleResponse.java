package com.nrodriguez.soaint.prueba.model.expose;

import lombok.Data;

@Data
public class VentaDetalleResponse {

    private Long idDetalleVenta;
    private Long idProducto;
    private String nombreProducto;
    private Double precioProducto;
}
