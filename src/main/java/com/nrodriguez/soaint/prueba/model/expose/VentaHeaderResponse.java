package com.nrodriguez.soaint.prueba.model.expose;

import lombok.Data;

import java.util.List;

@Data
public class VentaHeaderResponse {

    private Long idCliente;
    private Long idVenta;
    private String nombreCliente;
    private Long fechaVenta;
    List<VentaDetalleResponse> lstDetalle;

}
