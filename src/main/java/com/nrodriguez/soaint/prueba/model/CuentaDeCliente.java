package com.nrodriguez.soaint.prueba.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cuenta_de_cliente")
@Getter
@Setter
public class CuentaDeCliente {

    @Id
    @GeneratedValue
    private Long idCuenta;

    private String username;
    private String password;
    private Long idCliente;

}
