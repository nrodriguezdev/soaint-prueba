package com.nrodriguez.soaint.prueba.model.expose;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VentaResponse {

    private Long idVenta;
    private Long idCliente;
    private Long fecha;
}
