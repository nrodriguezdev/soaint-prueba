package com.nrodriguez.soaint.prueba.model.expose;

import lombok.Data;

@Data
public class DetalleCarritoRequest {

    private Long idVenta;
    private Long idProducto;

}
