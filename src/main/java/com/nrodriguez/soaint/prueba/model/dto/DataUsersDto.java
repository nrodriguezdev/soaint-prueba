package com.nrodriguez.soaint.prueba.model.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DataUsersDto {

    private String usuario;
    private String contrasenia;
    private String nombreCliente;
    private String apellido;
    private String dni;
    private String telefono;
    private String email;
}
