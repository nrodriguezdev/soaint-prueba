package com.nrodriguez.soaint.prueba.model.expose;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserCredentialsRequest {

    private String username;
    private String password;
}
