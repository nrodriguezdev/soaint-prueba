package com.nrodriguez.soaint.prueba.model.expose;

import lombok.Data;

@Data
public class ProductoRequest {

    private String nombreProducto;
    private Double precioProducto;
}
