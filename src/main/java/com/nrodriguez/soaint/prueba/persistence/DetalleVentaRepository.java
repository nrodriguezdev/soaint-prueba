package com.nrodriguez.soaint.prueba.persistence;

import com.nrodriguez.soaint.prueba.model.DetalleVenta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DetalleVentaRepository extends JpaRepository<DetalleVenta, Long> {

    @Query("SELECT T FROM DetalleVenta T WHERE T.idVenta = ?1")
    List<DetalleVenta> findAllDetalleByIdVenta(Long idVenta);
}
