package com.nrodriguez.soaint.prueba.persistence;

import com.nrodriguez.soaint.prueba.model.Venta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VentaRepository extends JpaRepository<Venta, Long> {

    @Query("SELECT T FROM Venta T WHERE T.idCliente = ?1")
    List<Venta> buscarVentasPorIdCliente(Long idCliente);

}
