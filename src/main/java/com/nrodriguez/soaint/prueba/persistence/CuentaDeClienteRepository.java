package com.nrodriguez.soaint.prueba.persistence;

import com.nrodriguez.soaint.prueba.model.CuentaDeCliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CuentaDeClienteRepository extends JpaRepository<CuentaDeCliente, Long> {

    @Query("SELECT T FROM CuentaDeCliente T WHERE T.username = ?1")
    CuentaDeCliente findByUsername(String username);
}
