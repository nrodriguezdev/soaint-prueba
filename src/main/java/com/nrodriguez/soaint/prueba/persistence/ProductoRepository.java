package com.nrodriguez.soaint.prueba.persistence;

import com.nrodriguez.soaint.prueba.model.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long> {

}
