package com.nrodriguez.soaint.prueba.utils.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EnumTokenExceptions {

    ERROR_GENERIC("EXC001", "Generic Error."),
    ERROR_TOKEN_EXPIRED("EXC002", "The sent token has expired."),
    ERROR_INTERNAL("EXC003", "Internal Error."),
    ERROR_USER_NOT_EXIST("EXC004", "Sent user does not exist."),
    ERROR_PASSWORD("EXC005", "Password is incorrect."),
    ERROR_DECODE("EXC006", "Decode error."),
    ERROR_TOKEN("EXC007", "Token is required.");

    private final String codeError;
    private final String messageError;
}
