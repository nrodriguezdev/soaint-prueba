package com.nrodriguez.soaint.prueba.utils;

public class Constants {

    public static final String APPLICATION_JSON = "application/json";
    public static final String UTF8 = "UTF-8";
    public static final String KEY_CODE = "codeError";
    public static final String KEY_MESSAGE = "messageError";
    public static final String KEY_HTTP_STATUS = "httpStatus";
}
