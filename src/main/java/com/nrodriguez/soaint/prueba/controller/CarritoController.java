package com.nrodriguez.soaint.prueba.controller;

import com.nrodriguez.soaint.prueba.model.expose.DetalleCarritoRequest;
import com.nrodriguez.soaint.prueba.model.expose.VentaHeaderResponse;
import com.nrodriguez.soaint.prueba.model.expose.VentaResponse;
import com.nrodriguez.soaint.prueba.service.CarritoService;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/carrito-compras")
public class CarritoController {

    private final CarritoService carritoService;

    @Autowired
    public CarritoController(CarritoService carritoService) {
        this.carritoService = carritoService;
    }

    @PostMapping("/crear-carrito")
    public ResponseEntity<VentaResponse> createVenta(ServletRequest requestToken) throws Exception {
        final HttpServletRequest req = (HttpServletRequest) requestToken;
        final String idCliente = (String) req.getAttribute("idCliente");
        return ResponseEntity.ok(carritoService.createVenta(Long.parseLong(idCliente)));
    }


    @PostMapping("/agregar-producto-carrito")
    public ResponseEntity<String> agregarProductoAlCarrito(@RequestBody DetalleCarritoRequest request) throws Exception {
        return ResponseEntity.ok(carritoService.agregarProductoAlCarrito(request));
    }

    @GetMapping("/buscar-venta/{idVenta}")
    public Single<ResponseEntity<VentaHeaderResponse>> buscarVentaPorIdVenta(@PathVariable Long idVenta) throws Exception {
        return carritoService.findByIdVenta(idVenta).subscribeOn(Schedulers.io()).map(ResponseEntity::ok);
    }

    @GetMapping("/buscar-venta-cliente")
    public Single<ResponseEntity<List<VentaHeaderResponse>>> buscarVentaPorIdCliente(ServletRequest requestToken) throws Exception {
        final HttpServletRequest req = (HttpServletRequest) requestToken;
        final String idCliente = (String) req.getAttribute("idCliente");
        return carritoService.findByIdCliente(Long.parseLong(idCliente)).subscribeOn(Schedulers.io()).map(ResponseEntity::ok);
    }
}
