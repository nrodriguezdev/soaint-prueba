package com.nrodriguez.soaint.prueba.controller;


import com.nrodriguez.soaint.prueba.model.expose.TokenResponse;
import com.nrodriguez.soaint.prueba.model.expose.UserCredentialsRequest;
import com.nrodriguez.soaint.prueba.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

    private final LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<TokenResponse> login(@RequestBody UserCredentialsRequest request) throws Exception {
        return ResponseEntity.ok(loginService.login(request));
    }
}
