package com.nrodriguez.soaint.prueba.controller;


import com.nrodriguez.soaint.prueba.model.Producto;
import com.nrodriguez.soaint.prueba.model.expose.ProductoRequest;
import com.nrodriguez.soaint.prueba.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/producto")
public class ProductoController {

    private final ProductoService productoService;

    @Autowired
    public ProductoController(ProductoService productoService) {
        this.productoService = productoService;
    }

    @PostMapping
    public ResponseEntity<Producto> registrarProducto(@RequestBody ProductoRequest request) throws Exception {
        return new ResponseEntity<>(productoService.registrarProducto(request), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> eliminarProducto(@PathVariable Long id) throws Exception {
        return ResponseEntity.ok(productoService.eliminarProducto(id));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Producto> consultarProducto(@PathVariable Long id) throws Exception {
        return ResponseEntity.ok(productoService.consultarProducto(id));
    }

    @GetMapping
    public ResponseEntity<List<Producto>> buscarTodosProducto() throws Exception {
        return ResponseEntity.ok(productoService.buscarTodosProducto());
    }
}
